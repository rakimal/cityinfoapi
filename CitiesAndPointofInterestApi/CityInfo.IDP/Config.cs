﻿using System.Collections.Generic;
using System.Security.Claims;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;

namespace CityInfo.IDP
{
    public static class Config
    {
        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>()
            {
                new TestUser()
                {
                    SubjectId = "d860efca-22d9-47fd-8249-791ba61b07c7",
                    Username = "Frank",
                    Password = "password",
                    Claims = new List<Claim>()
                    {
                        new Claim("given_name", "Frank"),
                        new Claim("family_name", "Underwood"),
                        new Claim("address", "Main Road 1"),
                        new Claim("role", "FreeUser"),                        
                        new Claim("city", "New York City")
                    }
                },
                new TestUser()
                {
                    SubjectId = "b7539694-97e7-4dfe-84da-b4256e1ff5c7",
                    Username = "Claire",
                    Password = "password",
                    Claims= new List<Claim>()
                    {
                        new Claim("given_name", "Claire"),
                        new Claim("family_name", "Underwood"),
                        new Claim("address", "Big Street 2"),
                        new Claim("role", "PayingUser"),                        
                        new Claim("city", "Paris")
                    }
                }
            };
        }

        //Claims are associated with Scopes.

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>()
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Address(),
                new IdentityResource("roles", "Your roles", new List<string>(){"role"}),
                new IdentityResource(
                    "city",
                    "The city you're living in",
                    new List<string>() { "city" })
            };
        }

        //api related resources socpes- This will add api resource in Access token
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>()
            {
                new ApiResource("cityinfoapi", "City Info API",
                    new List<string>() { "role", "city" })
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {               
                new Client
                {
                    ClientName="Postman Test Client",
                    ClientId= "postman-api",
                    AllowedGrantTypes= GrantTypes.Code,
                    RedirectUris= new List<string>
                    {
                        "https://www.getpostman.com/signin-oidc"
                    },
                    PostLogoutRedirectUris =new List<string>
                    {
                        "https://www.getpostman.com"
                    },
                    AllowedScopes=
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Address,
                        "roles",
                        "cityinfoapi",
                        "city"
                    },
                    ClientSecrets=
                    {
                        new Secret("secret".Sha256())
                    }

                }
            };
        }


    }
}