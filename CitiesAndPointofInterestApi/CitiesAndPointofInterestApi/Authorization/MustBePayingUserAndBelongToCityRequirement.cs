﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace CitiesAndPointofInterestApi.Authorization 
{
    public class MustBePayingUserAndBelongToCityRequirement : IAuthorizationRequirement
    {
        public MustBePayingUserAndBelongToCityRequirement()
        {
            
        }
    }
}
