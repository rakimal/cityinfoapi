﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CitiesAndPointofInterestApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CitiesAndPointofInterestApi.Authorization
{
    public class MustBePayingUserAndBelongToCityHandler : AuthorizationHandler<MustBePayingUserAndBelongToCityRequirement>
    {

        private ICityInfoRepository _cityInfoRepository;

        public MustBePayingUserAndBelongToCityHandler(ICityInfoRepository cityInfoRepository)
        {
            _cityInfoRepository = cityInfoRepository;
        }

        protected override Task HandleRequirementAsync(
            AuthorizationHandlerContext context, MustBePayingUserAndBelongToCityRequirement requirement)
        {
            var filterContext = context.Resource as AuthorizationFilterContext;

            if (filterContext == null)
            {
                context.Fail();
                return Task.CompletedTask;
            }

            int cityId = Convert.ToInt32(filterContext.RouteData.Values["cityId"]);

            var cityName = context.User.Claims.FirstOrDefault(c => c.Type == "city")?.Value;

            var role = context.User.Claims.FirstOrDefault(r => r.Type == "role")?.Value;

            var city = _cityInfoRepository.GetCity(cityId);

            if (city?.Name == cityName && role == "PayingUser")
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }

            context.Fail();
            return Task.CompletedTask;
        }
    }
}