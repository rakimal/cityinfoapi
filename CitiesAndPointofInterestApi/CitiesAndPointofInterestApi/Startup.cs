﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CitiesAndPointofInterestApi.Authorization;
using CitiesAndPointofInterestApi.Dtos;
using CitiesAndPointofInterestApi.Models;
using CitiesAndPointofInterestApi.Services;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CitiesAndPointofInterestApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddMvcOptions(o=>o.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter()));

            var connectionString = Configuration["connectionString:cityInfoDBConnectionString"];
            services.AddDbContext<CityInfoContext>(o=> o.UseSqlServer(connectionString));

            services.AddScoped<ICityInfoRepository, CityInfoRepository>();

            services.AddAuthorization(authorizationOptions =>
            {
                authorizationOptions.AddPolicy(
                    "MustBePayingUserAndBelongToCity",
                    policyBuilder =>
                    {
                        policyBuilder.RequireAuthenticatedUser();
                        policyBuilder.AddRequirements(
                            new MustBePayingUserAndBelongToCityRequirement());
                    });

            });

            services.AddScoped<IAuthorizationHandler, MustBePayingUserAndBelongToCityHandler>();

            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = "https://localhost:44391/";
                    options.ApiName = "cityinfoapi";
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseMvc();

            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<City, CityWithoutPointOfInterestDto>();
                cfg.CreateMap<City, CityDto>().ForMember(dest=>dest.PointsOfInterestList, opt=>opt.MapFrom(src=>src.PointsOfInterestsList));
                cfg.CreateMap<PointsOfInterest, PointsOfInterestDto>();
                cfg.CreateMap<PointsOfInterestForCreationDto, PointsOfInterest>();
                cfg.CreateMap<PointsOfInterestUpdateDto, PointsOfInterest>();
                cfg.CreateMap<PointsOfInterest, PointsOfInterestUpdateDto>();
            });

        }
    }
}
