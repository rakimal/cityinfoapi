﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CitiesAndPointofInterestApi.Dtos;
using CitiesAndPointofInterestApi.Models;
using CitiesAndPointofInterestApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CitiesAndPointofInterestApi.Controllers
{
   
    [Route("api/cities")]
    [Authorize]
    public class PointOfInterestController : Controller
    {

        private ICityInfoRepository _cityInfoRepository;
        private ILogger<PointOfInterestController> _logger;
        public PointOfInterestController(ICityInfoRepository cityInfoRepository, ILogger<PointOfInterestController> logger)
        {
            _cityInfoRepository = cityInfoRepository;
            _logger = logger;
        }

        [HttpGet("{cityId}/pointOfInterest")]
        public IActionResult GetPointOfInterest(int cityId)
        {
            if (!_cityInfoRepository.CityExists(cityId))
                return NotFound();

            var pointsOfInterest = _cityInfoRepository.GetPointsOfInterests(cityId);

            var results = Mapper.Map<IEnumerable<PointsOfInterestDto>>(pointsOfInterest);

            _logger.LogInformation($"Number of PointOfInterests returned: {results.Count()}");

            return Ok(results);

        }

        [HttpGet("{cityId}/pointofInterest/{pointOfInterestId}", Name = "GetPointOfInterestById")]
        public IActionResult GetPointOfInterestById(int cityId, int pointOfInterestId)
        {
            if (!_cityInfoRepository.CityExists(cityId))
                return NotFound();

            var pointOfInterest = _cityInfoRepository.GetPointOfInterestById(cityId, pointOfInterestId);

            if (pointOfInterest == null)
                return NotFound();

            var result = Mapper.Map<PointsOfInterestDto>(pointOfInterest);

            return Ok(result);
        }

        [HttpPost("{cityId}/pointofInterest")]
        [Authorize(Policy = "MustBePayingUserAndBelongToCity")]
        public IActionResult CreatePointOfInterest(int cityId, [FromBody]PointsOfInterestForCreationDto pointOfInterest)
        {

            if (pointOfInterest == null)
                return BadRequest();

            if (pointOfInterest.Description == pointOfInterest.Name)
            {
                ModelState.AddModelError("Description", "The provided description should be different from the name.");
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (!_cityInfoRepository.CityExists(cityId))
                return NotFound();

            var newPointofInterest = Mapper.Map<PointsOfInterest>(pointOfInterest);

            _cityInfoRepository.AddPointOfInterest(cityId, newPointofInterest);

            if (!_cityInfoRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            var createdPointOfInterest = Mapper.Map<PointsOfInterestDto>(newPointofInterest);

            return CreatedAtRoute("GetPointOfInterestById", new {cityId= cityId, pointOfInterestId = createdPointOfInterest.Id}, createdPointOfInterest);
        }

        [HttpPut("{cityId}/pointofInterest/{pointOfInterestId}")]
        public IActionResult UpdatePointOfInterest(int cityId, int pointOfInterestId,
            [FromBody] PointsOfInterestUpdateDto pointsOfInterest)
        {
            if (pointsOfInterest == null)
                return BadRequest();

            if (pointsOfInterest.Description == pointsOfInterest.Name)
            {
                ModelState.AddModelError("Description", "The provided description should be different from the name.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_cityInfoRepository.CityExists(cityId))
                return NotFound();

            var pointOfInterestToUpdate = _cityInfoRepository.GetPointOfInterestById(cityId, pointOfInterestId);

            if (pointOfInterestToUpdate == null)
                return NotFound();

            Mapper.Map(pointsOfInterest, pointOfInterestToUpdate);

            if (!_cityInfoRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            return NoContent();

        }

        [HttpPatch("{cityId}/pointofInterest/{pointOfInterestId}")]
        public IActionResult PartialUpdatePointOfInterest(int cityId, int pointOfInterestId,
            [FromBody] JsonPatchDocument<PointsOfInterestUpdateDto> patchDoc)
        {
            if (patchDoc == null)
                return BadRequest();

            if (!_cityInfoRepository.CityExists(cityId))
                return NotFound();

            var pointOfInterest = _cityInfoRepository.GetPointOfInterestById(cityId, pointOfInterestId);

            if (pointOfInterest == null)
                return NotFound();

            var pointofInterestToPatch = Mapper.Map<PointsOfInterestUpdateDto>(pointOfInterest);

            patchDoc.ApplyTo(pointofInterestToPatch, ModelState);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (pointofInterestToPatch.Description == pointofInterestToPatch.Name)
            {
                ModelState.AddModelError("Description", "The provided description should be different from the name.");
            }

            TryValidateModel(pointofInterestToPatch);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Map(pointofInterestToPatch, pointOfInterest);

            if (!_cityInfoRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            return NoContent();
        }

        [HttpDelete("{cityId}/pointofInterest/{pointOfInterestId}")]
        public IActionResult DeletePointOfInterest(int cityId, int pointOfInterestId)
        {
            if (!_cityInfoRepository.CityExists(cityId))
                return NotFound();

            var pointOfInterest = _cityInfoRepository.GetPointOfInterestById(cityId, pointOfInterestId);

            if (pointOfInterest == null)
                return NotFound();

            _cityInfoRepository.DeletePointOfInterest(pointOfInterest);

            if (!_cityInfoRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            return NoContent();
        }

    }
}