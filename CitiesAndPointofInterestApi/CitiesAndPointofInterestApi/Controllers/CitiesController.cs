﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CitiesAndPointofInterestApi.Dtos;
using CitiesAndPointofInterestApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CitiesAndPointofInterestApi.Controllers
{
    [Route("api/cities")]
    [Authorize]
    public class CitiesController : Controller
    {
        private ICityInfoRepository _cityInfoRepository;
        private ILogger<CitiesController> _logger;
        public CitiesController(ICityInfoRepository cityInfoRepository, ILogger<CitiesController> logger)
        {
            _cityInfoRepository = cityInfoRepository;
            _logger = logger;
        }
        
        // GET api/cities
        [HttpGet]
        public IActionResult Get()
        {
            var cities = _cityInfoRepository.GetCities();

            var results = Mapper.Map<IEnumerable<CityWithoutPointOfInterestDto>>(cities);

            _logger.LogInformation($"Total Cites returned: {results.Count()}");

            return Ok(results);
        }

        [HttpGet("{id}")]
        public IActionResult GetCity(int id)
        {
            var city = _cityInfoRepository.GetCity(id);

            if (city == null)
                return NotFound();

            var results = Mapper.Map<CityDto>(city);

            _logger.LogInformation($"City Name: {results.Name}");

            return Ok(results);

        }

       
    }
}
