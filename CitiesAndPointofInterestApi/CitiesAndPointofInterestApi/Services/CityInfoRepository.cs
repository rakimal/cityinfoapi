﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using CitiesAndPointofInterestApi.Models;
using Microsoft.EntityFrameworkCore;

namespace CitiesAndPointofInterestApi.Services
{
    public class CityInfoRepository : ICityInfoRepository
    {

        private CityInfoContext _context;
        public CityInfoRepository(CityInfoContext context)
        {
            _context = context;
        }

        public IEnumerable<City> GetCities()
        {
            return _context.Cities.OrderBy(c => c.Name).ToList();
        }

        public City GetCity(int cityId)
        {
            return _context.Cities.Include(c=>c.PointsOfInterestsList).FirstOrDefault(c => c.Id == cityId);
        }

        public bool CityExists(int cityId)
        {
            return _context.Cities.Any(c => c.Id == cityId);
        }

        public IEnumerable<PointsOfInterest> GetPointsOfInterests(int CityId)
        {
            return _context.PointsOfInterest.Where(c => c.CityId == CityId);
        }

        public PointsOfInterest GetPointOfInterestById(int cityId, int pointOfInterestId)
        {
            return _context.PointsOfInterest.FirstOrDefault(c => c.CityId == cityId && c.Id == pointOfInterestId);
        }

        public void AddPointOfInterest(int cityId, PointsOfInterest pointOfInterest)
        {
            var city = GetCity(cityId);

            city.PointsOfInterestsList.Add(pointOfInterest);
        }


        public void DeletePointOfInterest(PointsOfInterest pointsOfInterest)
        {
            _context.PointsOfInterest.Remove(pointsOfInterest);
          
        }

        public bool Save()
        {                       
            return (_context.SaveChanges() >= 0);                                 
        }



    }
}