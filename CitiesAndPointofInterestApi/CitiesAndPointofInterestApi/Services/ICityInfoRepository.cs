﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CitiesAndPointofInterestApi.Models;

namespace CitiesAndPointofInterestApi.Services
{
    public interface ICityInfoRepository
    {
        IEnumerable<City> GetCities();

        City GetCity(int cityId);

        IEnumerable<PointsOfInterest> GetPointsOfInterests(int CityId);

        bool CityExists(int cityId);

        PointsOfInterest GetPointOfInterestById(int cityId, int pointOfInterestId);

        void AddPointOfInterest(int cityId, PointsOfInterest pointOfInterest);

        void DeletePointOfInterest(PointsOfInterest pointsOfInterest);

        bool Save();

    }
}
