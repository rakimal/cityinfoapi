﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CitiesAndPointofInterestApi.Models;

namespace CitiesAndPointofInterestApi.Dtos
{
    public class CityDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int PointsOfInterestCount
        {
            get { return PointsOfInterestList.Count; }
        }

        public ICollection<PointsOfInterestDto> PointsOfInterestList { get; set; }= new List<PointsOfInterestDto>();
    }
}
