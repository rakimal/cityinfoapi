﻿using System.ComponentModel.DataAnnotations;

namespace CitiesAndPointofInterestApi.Dtos
{
    public class PointsOfInterestUpdateDto
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string Description { get; set; }
    }
}